import tkinter as tk
from time import sleep
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from threading import Thread
import csv
import time
import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import datetime as dt
from datetime import datetime
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import matplotlib.dates as dates



#####################################_______Flag_______##################################################

update_graph = False
update_scrap = False
graph_is_hidden = False
update_last_value = False



###############################____When we push button____###############################################

def on_click_button_show_and_hide_last_value(event):
    global update_last_value
    if update_last_value:
        update_last_value = False
        button_show_and_hide_last_value_var.set("Voir la dernière valeur du taux")


    else:
        update_last_value = True
        button_show_and_hide_last_value_var.set("Arreter d'afficher le taux en temps réel")
        global thread_2
        thread_2 = Thread(target=update_value_thread)
        thread_2.start()
    print(" Someone wants to display the last value ")

def on_click_button_updating_scrap(event):
    global update_scrap
    if update_scrap:
        update_scrap = False
        button_updating_scrap_var.set("Démarrer le scraping")
    else:
        update_scrap = True
        button_updating_scrap_var.set("Stoper le scraping")
        global thread_1
        thread_1 = Thread(target=scrap_thread)
        thread_1.start()
    print("Someone click for the scrap", event.x, event.y)

def on_click_button_updating_graph(event):
    global update_graph
    if update_graph:
        update_graph = False
        button_updating_graph_var.set("Lancer le graphique")
    else:
        update_graph = True
        button_updating_graph_var.set("Stoper le graphique")
        refresh()

    print("Someone click for the graph!", event.x, event.y)


def on_click_button_hide_and_show_graph(event):
    global graph_is_hidden
    if graph_is_hidden:
        graph_is_hidden = False
        button_hide_and_show_graph_var.set("Masquer le graphique")
        canvas.get_tk_widget().pack()

    else:
        graph_is_hidden = True
        button_hide_and_show_graph_var.set("Montrer le graphique")
        canvas.get_tk_widget().pack_forget()

def on_closing():
    global update_scrap
    global update_last_value
    global graph_is_hidden
    global update_graph
    update_scrap = False
    print("\n We've been lucky !\n")
    update_last_value = False
    print("\n We've been lucky x2 !\n")
    graph_is_hidden = False
    update_graph = False
    app.destroy()


###############################____Essential functions____###############################################



def get_new_x_y():
    PATH_FILE_NAME = "valeur_bourse_16.csv"
    df = pd.read_csv(PATH_FILE_NAME, sep=";")
    df["time"] = df["time"].astype(str)
    df["time"] = df["time"].apply(lambda x: datetime.fromisoformat(x))
    df = df.sort_values('time')
    x = df["time"].to_list()
    y = df["price"].to_list()
    return x, y

def my_personal_spyder_scraping():
    print("I run ")
    with open('valeur_bourse_16.csv', 'a', newline='') as csvfile:
        ecriture = csv.writer(csvfile, delimiter=';')
        valeur_finale = []
        tmp_list_1 = []
        tmp_list_2 = []
        pause = 5
        time.sleep(pause)
        page = requests.get("https://www.boursorama.com/bourse/devises/taux-de-change-euro-dollar-EUR-USD/")
        soup = BeautifulSoup(page.text, features="html.parser")
        valeur_bourse = soup.find("div", attrs={"class": "c-faceplate__price c-faceplate__price--inline"}).find("span", attrs={"class": "c-instrument c-instrument--last"})
        valeur_recup = valeur_bourse.get_text()
        now = datetime.now()
        the_time = now.strftime("%Y-%m-%d %H:%M:%S")
        tmp_list_1.append(valeur_recup)
        tmp_list_2.append(the_time)
        valeur_finale = tmp_list_1 + tmp_list_2
        print(valeur_finale)
        ecriture.writerow(valeur_finale)
        return valeur_finale

def get_last_value():
    while update_last_value:
        print("----------------------------")
        if update_scrap:
            my_personal_spyder_scraping()
            valeur_finale = my_personal_spyder_scraping()
        else:
            break

        return valeur_finale

def scrap_thread():
    global update_scrap
    print("I launch my  thread")

    while True:
        print("----------------------------")
        if update_scrap:
            my_personal_spyder_scraping()
        else:
            break


    print("j'ai fini mon thread")

def update_value_thread():
    global update_last_value
    print("thread2")
    while True:
        print("111111111111111111111111111111111")
        if update_last_value:
            valeur_finale = get_last_value()
            if valeur_finale != None:
                label_show_value_var.set("Dernière donnée : {}".format(valeur_finale))
            else:
                print(" No values to display")
                sleep(3)
                pass

        else:
            break
    print("thread 2 finished")

def refresh():
    global update_graph
    if not update_graph:
        print(" STOP ")
        return
    global nb_plot_counter
    print("I'm refresh")
    x, y = get_new_x_y()
    print(x)
    print(y)
    print("################################")
    ax.lines.pop(0)  # detruit l'ancienne ligne
    ax.plot(x, y)
    canvas.draw()

    app.after(2000, refresh)  # call la fonction apres 2000 ms

def create_window_and_predict():
    
    newWindow = tk.Toplevel(app)
    
    #Récupération et organisation des datas
    
    dataset = pd.read_csv("valeur_bourse_16.csv", sep=";")
    dataset['time'] = pd.to_datetime(dataset['time'])
    dataset["time"] = dataset["time"].astype(str)
    dataset["time"] = dataset["time"].apply(lambda x: datetime.fromisoformat(x))
    dataset = dataset.set_index('time')
    dataset = dataset.sort_values('time')
    print(dataset.head())
    print(dataset.info())

    print(dataset.isna().sum())
    # dataset.dropna(axis=0, inplace=True)
    print(dataset.head(10))

    min_date = dataset.index.min()
    max_date = dataset.index.max()
    print("Début :", min_date)
    print("Fin :", max_date)
    print("Durée du scrap :", max_date - min_date)

    # Organisation des données
    # num = 231
    # dataset['label'] = dataset['price'].shift(-num)
    temp_list = dataset['price'].to_list()
    temp_list.pop(0)

    dataset = dataset[:-1]
    dataset['t_1'] = temp_list

    temp_list = dataset['t_1'].to_list()
    temp_list.pop(0)
    dataset = dataset[:-1]
    dataset['t_2'] = temp_list

    print(dataset.shape)

    X = dataset['price'].values
    X = preprocessing.scale(X)
    # X = X[:-num]

    # dataset.dropna(inplace=True)
    y = dataset['t_1'].values

    print(np.shape(X), np.shape(y))

    # Entrainement de la machine (regression lineaire)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    X_train_2array = X_train.reshape(-1, 1)
    y_train_2array = y_train.reshape(-1, 1)

    X_test_2array = X_test.reshape(-1, 1)
    y_test_2array = y_test.reshape(-1, 1)

    lr = LinearRegression()
    lr.fit(X_train_2array, y_train_2array)
    print(lr.score(X_test_2array, y_test_2array))

    # Prédiction des données
    X_to_test_2array = dataset['price']  # récupère les données d'une colonne (ici c'est 'price')
    X_to_test_2array = X_to_test_2array.tolist()  # transforme l'object Series en list
    X_to_test_2array = np.array(X_to_test_2array)  # transforme une liste en np.array
    X_to_test_2array = X_to_test_2array.reshape(-1, 1)  # [1,2,3] => [[1],[2],[3]]
    y_predict_2array = lr.predict(X_to_test_2array)

    datetime_of_predicted_values_list = dataset.index.tolist()
    datetime_of_predicted_values_list.pop(0)
    datetime_of_predicted_values_list.append(datetime_of_predicted_values_list[-1] + dt.timedelta(0, 5))  # triche (penser à ajouter 5 secondes)
    datetime_of_predicted_values_list = dates.date2num(datetime_of_predicted_values_list)
    
    """
    plt.plot(datetime_of_predicted_values_list, y_predict_2array)
    plt.xlabel('time')
    plt.ylabel('price')
    plt.show()
    """

    fig3 = plt.figure(facecolor="#F0FFF0", figsize=[15, 8])
    ax3 = fig3.add_subplot(111)

    ax3.plot(datetime_of_predicted_values_list, y_predict_2array, color='red', label='Prédiction à t+5s')

    ax3.set_xlabel('Temps')
    ax3.set_ylabel('Prix')
    ax3.set_title("Prédictions à T+5s")
    plt.legend(loc='best')
    canvas_3 = FigureCanvasTkAgg(fig3, newWindow)
    canvas_3.get_tk_widget().pack()
    canvas_3.draw()

#######################################_____Statistiques________#########################################

def statistiques():
    
    newWindow2 = tk.Toplevel(app)
    PATH_FILE_NAME_2 = "valeur_bourse_16.csv"
    df_2 = pd.read_csv(PATH_FILE_NAME_2, sep=";")

    df_2 = df_2.set_index(pd.DatetimeIndex(df_2['time']))

    stats = df_2.resample('5Min').agg(['mean', 'std', 'min', 'max'])
    stats.dropna(axis=0, inplace = True)
    
    print(stats.head(5))



    the_mean = stats[('price','mean')]
    val_min = stats[('price','min')]
    #std_val = stats[('price','std')]
    val_max = stats[('price','max')]

    fig2 = plt.figure(facecolor="#F0FFF0", figsize=[15, 8])
    ax2 = fig2.add_subplot(1,1,1)


    ax2.plot(the_mean, color='red', label='moyenne par 5 min ')
    ax2.plot(val_min, color='yellow', label='min')
    ax2.plot(val_max, color='green', label='max')

    ax2.fill_between(stats.index,val_min, val_max, alpha =0.2, label = 'min/max')
    ax2.set_title('Statistiques ')
    ax2.set_xlabel('Temps')
    ax2.set_ylabel('Prix')
    plt.legend()
    
    canvas_2 = FigureCanvasTkAgg(fig2, newWindow2)
    canvas_2.get_tk_widget().pack()
    canvas_2.draw()



###############################____Tkinter creation____###############################################
app = tk.Tk()





frame_top = tk.Frame()
frame_top.pack(side='top')

app.title("Notre application boursière ")
#app.iconbitmap("logo.ico")
app.config(background="#5F9EA0")


label = tk.Label(frame_top, text="Etude du cours EUR/USD", font = ("Courrier", 20), bg ="#5F9EA0", fg = "#F0FFF0")
label.pack(fill='both')

frame_bottom = tk.Frame()
frame_bottom.config(background ="#5F9EA0")
frame_bottom.pack(side='bottom')

button_show_forecast = tk.Button(frame_bottom, text="Afficher les prévisons", font = ("Courrier", 15), fg = "#5F9EA0", command=create_window_and_predict)
button_show_forecast.pack(side='bottom')

button_show_stats = tk.Button(frame_bottom, text="Afficher les statistiques", font = ("Courrier", 15), fg = "#5F9EA0", command=statistiques)
button_show_stats.pack(side='bottom')

button_updating_graph_var = tk.StringVar()
button_updating_graph = tk.Button(frame_top, textvariable=button_updating_graph_var, font = ("Courrier", 15), fg = "#5F9EA0")  # on déclare le bouton et on le bind au tk.StringVar()
button_updating_graph.pack(side='left')
button_updating_graph_var.set("Lancer le graphique")



button_updating_scrap_var = tk.StringVar()
button_updating_scrap = tk.Button(frame_top, textvariable=button_updating_scrap_var,  font = ("Courrier", 15), fg = "#5F9EA0")
button_updating_scrap.pack(side='left')
button_updating_scrap_var.set("Lancer le scraping")



button_show_and_hide_last_value_var = tk.StringVar()
button_show_and_hide_last_value = tk.Button(frame_top, textvariable=button_show_and_hide_last_value_var,  font = ("Courrier", 15), fg = "#5F9EA0")
button_show_and_hide_last_value.pack(side='left')
button_show_and_hide_last_value_var.set("Voir la dernière valeur du taux ")


valeur_finale = "Aucune donnée pour le moment"
label_show_value_var = tk.StringVar()
label_show_value = tk.Label(frame_bottom, textvariable=label_show_value_var, font=("Courrier", 15), bg="#5F9EA0", fg="#F0FFF0")
label_show_value.pack(fill='both')
label_show_value_var.set("Dernière donnée : {}".format(valeur_finale))


button_hide_and_show_graph_var = tk.StringVar()
button_hide_and_show_graph = tk.Button(frame_top, textvariable=button_hide_and_show_graph_var, font = ("Courrier", 15), fg = "#5F9EA0")
button_hide_and_show_graph.pack(side='left')
button_hide_and_show_graph_var.set("Cacher le graphique")

frame_center = tk.Frame()
frame_center.pack(side='top')
frame_center.config(background ="#5F9EA0")


fig = plt.figure(facecolor="#F0FFF0", figsize=[15, 8])

canvas = FigureCanvasTkAgg(fig, frame_center)
canvas.get_tk_widget().pack() #Recupere le widget tkinter de la figure canva.tk.get...
ax = fig.add_subplot(111)
ax.set_ylabel('Taux de change EUR/USD')
ax.set_xlabel('Temps (jour et heure)')
ax.set_title("Taux de change EUR / USD en fonction du temps")
ax.plot([], [])



button_show_and_hide_last_value.bind("<ButtonRelease-1>", on_click_button_show_and_hide_last_value)
button_updating_graph.bind("<ButtonRelease-1>", on_click_button_updating_graph)


button_updating_scrap.bind("<ButtonRelease-1>", on_click_button_updating_scrap)
button_hide_and_show_graph.bind("<ButtonRelease-1>", on_click_button_hide_and_show_graph)

app.protocol("WM_DELETE_WINDOW", on_closing)


app.mainloop()
